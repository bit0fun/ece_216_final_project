/*
 * ECE216_Final_Lab.c
 *
 * Created: 11/22/2016 2:01:48 PM
 * Author : bit0fun
 */ 

#include <avr/io.h>
#define F_CPU 16000000UL	//16MHz clock in, needed for delay functions
#include <util/delay.h>
#include <avr/interrupt.h>

//general defines
#define FORWARD			1
#define BACKWARD		0
#define ENABLE			1
#define DISABLE			0
#define DNC				-1
#define ON				1
#define OFF				0
#define TURN_TIME		2000	//time required for turning, in ms. Change as needed
#define LOOP_CYCLE		5		//needed for Ultrasonic sensor timing. Need to disassemble code to get correct value, or use inline assembly instead


//Ultrasonic Sensor
#define ECHO			PORTB0		//input
#define TRIG			PORTC5		//output


//Servo
#define SERVO			PORTB1		//PWM output

//LEDs
#define OBST			PORTC0		//output
#define	HIT				PORTC1		//output
#define ERR				PORTC4		//output

//LDR
#define	ROBOT_DET		PORTD7		//input


//Motor Control
#define EN_L			PORTD0		//output
#define EN_R			PORTD1		//output
#define	MTL_1			PORTD2		//output
#define MTL_2			PORTD3		//output
#define MTR_1			PORTD4	    //output
#define	MTR_2			PORTD5		//output


//IR Connections
#define IR_IN			PORTC3		//input
#define IR_OUT			PORTC2		//output


//Masks for checking input, setting direction registers

//Inputs
#define LDR_MASK		0x80 //for checking input of LDR
#define IR_MASK			0x08 //for checking input of IR Detector
#define ULT_SON_MASK	0x20 //for getting pulse width of Ultrasonic Sensor

//Direction Settings: if 0, input, if 1, output
#define PORTB_MASK		0x02// 0000 0010
#define PORTC_MASK		0x37// 0011 0111
#define PORTD_MASK		0xBF// 1011 1111
				 		

//Functions
void move_straight();								//moves the robot forward
void turn_right(int sharp);							//turns the robot to the right, can set if a sharp turn or not
void turn_left(int sharp);							//turns the robot to the left, can set if a sharp turn or not
void brake(); 										//brakes for short period of time, then releases brake
void brake_en(int hard);							//enables break, hard or roll
void brake_dis();									//disables break
void left_motor(int direction, int enable);			//controls the direction the left motor moves as well as the enable pin
void right_motor(int direction, int enable);		//controls the direction the right motor moves as well as the enable pin
int search();										//searches for obstacle, returns distance if obstacle found
int get_distance();									//gets distance from ultrasonic sensor in cm
void set_led(int led_name);							//sets the LED passed to function
void clr_led(int led_name);							//clears the LED passed to function
int det_robot();									//uses LDR and Ultrasonic sensor and returns 1 if robot found, otherwise 0
int ldr_detect();									//checks LDR pin to see if light is present

int main(void) {
   
	//Setting up IO for Inputs and Outputs
	DDRB = PORTB_MASK;
	DDRC = PORTC_MASK;
	DDRD = PORTD_MASK;
	
	sei(); //enabling interrupts
	
   
    while (1) {
		
    }
}


//Functions

//moves the robot forward
void move_straight(){								
	right_motor(FORWARD, ENABLE);
	left_motor(FORWARD, ENABLE);
	_delay_ms(200);
	
	//resets motors to not moving
	right_motor(DNC, DISABLE);
	left_motor(DNC, DISABLE);
	return;
}

//turns the robot to the right, can set if a sharp turn or not
void turn_right(int sharp){							
	if(sharp){
		right_motor(FORWARD, ENABLE);
		left_motor(BACKWARD, ENABLE);
	}
	else{
		right_motor(DNC, DISABLE);	//disables left motor
		left_motor(FORWARD, ENABLE);
		
	}
	_delay_ms(TURN_TIME);	//delay required for turning
	
	//resets motors to not moving
	right_motor(DNC, DISABLE);
	left_motor(DNC, DISABLE);
	
	return;
}

//turns the robot to the left, can set if a sharp turn or not
void turn_left(int sharp){							
	if(sharp){
		left_motor(FORWARD, ENABLE);
		right_motor(BACKWARD, ENABLE);
	}
	else{
		left_motor(DNC, DISABLE);	//disables left motor
		right_motor(FORWARD, ENABLE);
		
	}
	_delay_ms(TURN_TIME);	//delay required for turning

	//resets motors to not moving
	right_motor(DNC, DISABLE);
	left_motor(DNC, DISABLE);

	return;
}

//brakes for short period of time, then releases brake
void brake(){ 				

	brake_en();
	_delay_ms(100);
	brake_dis();
	return;
}

//enables break, hard or roll
void brake_en(int hard){									
	int mtr_state;
	if(hard){
		mtr_state = (1 << EN_L) | (1 << EN_R) | ~(1<<MTL_1) | ~(1<<MTL_2) | ~(1<<MTR_1) | ~(1<<MTR_2);
		PORTD = mtr_state | (PIND7); //keeps state of LDR
		return;
	}
	else{
		PORTD &= ~(1<< EN_L) | ~(1 << EN_R);
		return;
	}
	
}

//disables break
void brake_dis(){									
	PORTD = 0x00 | (PIND7);	//keeps state of LDR
	return;
}

//controls the direction the left motor moves as well as the enable pin
void left_motor(int direction, int enable){			
	switch direction	//sets direction to forward, backward, or don't care
		case	FORWARD:	PORTD |= (1<<MTL_1);
							PORTD &= ~(1<<MTL_2);
							break;
		case	BACKWARD:	PORTD |= (1<<MTL_2);
							PORTD &= ~(1<<MTL_1);
							break;
		case	DNC:
		
	if (enable){
		PORTD |= (1 << || EN_L); //enables left motor to move
	}
	return;
}

//controls the direction the right motor moves as well as the enable pin
void right_motor(int direction, int enable){		
	switch direction	//sets direction to forward, backward, or don't care
		case	FORWARD:	PORTD |= (1<<MTR_1);
							PORTD &= ~(1<<MTR_2);
							break;
		case	BACKWARD:	PORTD |= (1<<MTR_2);
							PORTD &= ~(1<<MTR_1);
							break;
		case	DNC:
							break;
	
	if (enable){
		PORTD |= (1 << || EN_R); //enables left motor to move
	}
	return;
}

//searches for obstacle, returns distance if obstacle found
int search(){										
	int distance = get_distance();
	int threshold = 30; //cm ; set for recognizing distance where object is recognized
	if(threshold > distance){
		return distance; //if the distance is less than the threshold, obstacle is found
	}
	else{
		return 0;	//nothing found, exit function
	}
}

//gets distance from ultrasonic sensor in cm
int get_distance(){									
	float distance = 0;			//variable for end result
	float raw_distance = 0;		//variable for getting output of ultrasonic sensor
	
	/* Code for using Ultrasonic Sensor*/
	PORTB |= (1<< ECHO); //Start 10 us pulse
	_delay_us(10);
	PORTB &= ~(1<<ECHO);
	while((PINC & (1<<TRIG)) == 0); //wait for pulse to come back
	while((PINC & (1<<TRIG)) == 1){
		raw_distance++;	//increment counter while pulse is high to count how long the pulse is
	}
	raw_distance *= (1/F_CPU) * LOOP_CYCLE; //returns time in seconds
	raw_distance/=2;
	distance = 343/*m/s*/ * raw_distance; //raw distance times speed of sound gives answer of distance
	return (int)(distance * 100); //cast as int, don't need decimals, nor that much precision. multiplied by 100 to return cm instead of m
}

//sets the LED passed to function
void set_led(int led_name){							
	PORTC |= (1<<led_name);
	return;
}

//clears the LED passed to function
void clr_led(int led_name){							
	PORTC &= ~(1<<led_name);
	return;
}

//uses LDR and Ultrasonic sensor and returns 1 if robot found, otherwise 0
int det_robot(){									
	int found_robot = 0;
	int obst_detect = search();
	int ldr_check  = ldr_detect();
	
	if(ldr_check && obst_detect){
		found_robot = 1;
	}
	
	
	return found_robot;
}

//checks LDR pin to see if light is present
int ldr_detect(){									
	int detect = 0;						//variable for detecting light present
	if((LDR_MASK & PIND) == 1){		//If the LDR pin is high
		detect = 1;
	}
	return detect;
}